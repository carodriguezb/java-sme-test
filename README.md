
# Christian Rodríguez - Java SME Test
This project contains the code done for the Java SME Test.
## Summary
The project was done using:

 - **Gradle** for build and automation tasks.
 - **Spring boot** for project setup and initial configurations. 
 - **Spring web** for publishing endpoints for the UI layer.
 - **Spring websockets** for handling notifications asynchronously.
 - **Spring batch** for pages processing in batch job.
 - **Spring Devtools** for faster reload and debug.
 - **Thymeleaf** for UI templating.
 - **Webdriver and Jsoup** for page fetching and scraping.
 - **H2** for supporting spring batch persistency.

## Instructions
 In order to run the app you should follow the next steps:   
 
 1. Clone the code to your computer.
 2. Configure the following properties in the `application.properties` file according to your local environment:
	 -  `app.upload-folder`: is the path where the output files will be generated.
	 - 	 `app.webdriver.driver.path`: is the path where the chrome webdriver executable is located, this repo contains a windows driver in case you don't have one downloaded locally.
 3.  Execute the app using `gradlew clean bootRun`
 4. Open the app in your browser [http://localhost:8080](http://localhost:8080)

## Author
Christian Rodriguez <cristian3100@gmail.com>
