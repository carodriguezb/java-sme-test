var stompClient = null;

function connect() {
    var socket = new SockJS('/queue');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/queue/notifications', function (notification) {
            var notificationJson = JSON.parse(notification.body);
            setTimeout(function () {
                showNotification(notificationJson);
            }, 500 * notificationJson.notificationDelay);

        });
    });
}

function showNotification(notification) {
    var url = notification.message.replace(/\\/g, '_');
    $('#files-generated').show();
    $('#files-generated').append('<a href="/files/download/' + url + '" target="_blank">' + notification.message + '</a>');
    $('#files-generated').append('<br/>');
}
