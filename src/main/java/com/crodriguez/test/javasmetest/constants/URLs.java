package com.crodriguez.test.javasmetest.constants;

/**
 * Constant class used to define the urls and templates used in the app.
 */
public class URLs {

    public static final String NOTIFICATIONS_URL = "/queue/notifications";
    public static final String WEB_SOCKET_ENDPOINT_URL = "/queue";
    public static final String FILE_TEMPLATE = "file";
}
