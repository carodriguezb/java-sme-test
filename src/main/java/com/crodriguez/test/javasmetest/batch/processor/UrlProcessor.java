package com.crodriguez.test.javasmetest.batch.processor;

import com.crodriguez.test.javasmetest.dto.batch.UrlResultsDTO;
import com.crodriguez.test.javasmetest.enumeration.PatternEnum;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Value;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * File processor responsible for visiting the site fetch the visible text and search for required patterns. The load
 * of the site is done using a headless chrome driver and parsing is using a Jsoup doc reader.
 */
public class UrlProcessor implements ItemProcessor<String, UrlResultsDTO> {

    private String patterns;

    @Value("${app.webdriver.driver.path}")
    private String DRIVER_PATH;

    public UrlProcessor(String patterns) {
        this.patterns = patterns;
    }

    @Override
    public UrlResultsDTO process(String url) {

        String pageText = getPageText(url);

        UrlResultsDTO results = new UrlResultsDTO();
        results.setUrl(url);

        if (patterns.contains(PatternEnum.HASH.getValue())) {
            results.setHashtags(getHashTags(pageText));
        }

        if (patterns.contains(PatternEnum.ACCOUNT.getValue())) {
            results.setAccounts(getAccounts(pageText));
        }

        return results;
    }

    /**
     * Fetch the page content using webdriver and Jsoup.
     *
     * @param url String with the site url.
     * @return String with the page content.
     */
    private String getPageText(String url) {
        WebDriver driver = getHeadlessDriver();
        driver.get(url);
        Document doc = Jsoup.parse(driver.getPageSource());
        driver.close();
        return doc.body().text();
    }

    /**
     * Search for hash tags in the provided string using regex.
     *
     * @param pageText String with the page content.
     * @return List with the  hash tags found.
     */
    private LinkedList<String> getHashTags(String pageText) {
        LinkedList<String> matches = new LinkedList<>();
        Matcher m = Pattern.compile("(?<=^|(?<=[^a-zA-Z0-9-_\\.]))#([A-Za-z]+[A-Za-z0-9-_]+)").matcher(pageText);
        while (m.find()) {
            matches.add(m.group());
        }
        return matches;
    }

    /**
     * Search for accounts in the provided string using regex.
     *
     * @param pageText String with the page content.
     * @return List with the accounts found.
     */
    private LinkedList<String> getAccounts(String pageText) {
        LinkedList<String> matches = new LinkedList<>();
        Matcher m = Pattern.compile("(?<=^|(?<=[^a-zA-Z0-9-_\\.]))@([A-Za-z]+[A-Za-z0-9-_]+)").matcher(pageText);
        while (m.find()) {
            matches.add(m.group());
        }
        return matches;
    }

    /**
     * Generate a new headless chrome driver.
     *
     * @return generated headless Chrome driver.
     */
    private WebDriver getHeadlessDriver() {
        System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        return new ChromeDriver(chromeOptions);
    }
}
