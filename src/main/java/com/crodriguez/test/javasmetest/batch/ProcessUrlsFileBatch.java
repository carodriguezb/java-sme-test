package com.crodriguez.test.javasmetest.batch;

import com.crodriguez.test.javasmetest.batch.aggregator.UrlResultsAcountsAggregator;
import com.crodriguez.test.javasmetest.batch.aggregator.UrlResultsHashAggregator;
import com.crodriguez.test.javasmetest.batch.aggregator.UrlResultsNamesAggregator;
import com.crodriguez.test.javasmetest.batch.processor.UrlProcessor;
import com.crodriguez.test.javasmetest.batch.reader.UrlsFileReader;
import com.crodriguez.test.javasmetest.batch.writer.UrlResultsFileWriter;
import com.crodriguez.test.javasmetest.dto.batch.UrlResultsDTO;
import com.crodriguez.test.javasmetest.enumeration.PatternEnum;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Batch job definition used to process the urls file and generate the output files.
 */
@Configuration
@EnableBatchProcessing
public class ProcessUrlsFileBatch {

    public static final String PARAM_INPUT_FILE_PATH = "inputFilePath";
    public static final String PARAM_OUTPUT_PATH = "outputPath";
    public static final String PARAM_PATTERNS = "patterns";
    private static final String BATCH_NAME = "ProcessUrlsFileJob";
    private static final String OVERRIDDEN_BY_PARAMETER = null;

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final SimpMessagingTemplate messagingTemplate;

    @Autowired
    public ProcessUrlsFileBatch(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, SimpMessagingTemplate messagingTemplate) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.messagingTemplate = messagingTemplate;
    }

    /**
     * Defines the processing job and the steps that will be executed.
     *
     * @return The Job definition.
     */
    @Bean(name = BATCH_NAME)
    public Job processUrlsFileJob() {
        return this.jobBuilderFactory
                .get(BATCH_NAME)
                .incrementer(new RunIdIncrementer())
                .flow(ProcessUrlsFileStep())
//                .listener()
                .end()
                .build();
    }

    /**
     * Defines the step used to process the urls file, this method describe the reader, processor and writer
     * used in the step.
     *
     * @return The Step definition.
     */
    private Step ProcessUrlsFileStep() {
        return stepBuilderFactory.get("ProcessUrlsFileStep")
                .<String, UrlResultsDTO>chunk(1)
                .reader(reader(OVERRIDDEN_BY_PARAMETER))
                .processor(processor(OVERRIDDEN_BY_PARAMETER))
                .writer(writer(OVERRIDDEN_BY_PARAMETER, OVERRIDDEN_BY_PARAMETER))
                .build();
    }

    /**
     * Bean used to define the reader of the url file. See {@link UrlsFileReader}
     *
     * @param inputFilePath String with the job parameter that represents the path of the input file.
     * @return Bean with the reader definition.
     */
    @Bean
    @StepScope
    public FlatFileItemReader<String> reader(@Value("#{jobParameters[" + PARAM_INPUT_FILE_PATH + "]}") final String inputFilePath) {
        return new UrlsFileReader(inputFilePath);
    }

    /**
     * Bean used to define the processor of the url file. See {@link UrlProcessor}
     *
     * @return Bean with the processor definition.
     */
    @Bean
    @StepScope
    public UrlProcessor processor(@Value("#{jobParameters[" + PARAM_PATTERNS + "]}") final String patterns) {
        return new UrlProcessor(patterns);
    }

    /**
     * Bean used to define the composition of the writers used to generate the different output files.
     * See {@link UrlResultsFileWriter}, {@link UrlResultsFileWriter} and {@link UrlResultsFileWriter}.
     *
     * @param outputPath String with the job parameter that represents the path where the output files should be generated.
     * @param patterns   String with the concatenation of the patterns selected by the users. See {@link PatternEnum}.
     * @return Bean with the composition of the writers defined.
     */
    @Bean
    @StepScope
    public CompositeItemWriter writer(@Value("#{jobParameters[" + PARAM_OUTPUT_PATH + "]}") final String outputPath,
                                      @Value("#{jobParameters[" + PARAM_PATTERNS + "]}") final String patterns) {


        CompositeItemWriter compWriter = new CompositeItemWriter();
        List<ItemWriter> itemWriters = new ArrayList<>();

        if (patterns.contains(PatternEnum.ACCOUNT.getValue())) {
            itemWriters.add(new UrlResultsFileWriter(outputPath, PatternEnum.ACCOUNT, new UrlResultsAcountsAggregator(), messagingTemplate));
        }

        if (patterns.contains(PatternEnum.HASH.getValue())) {
            itemWriters.add(new UrlResultsFileWriter(outputPath, PatternEnum.HASH, new UrlResultsHashAggregator(), messagingTemplate));
        }

        if (patterns.contains(PatternEnum.NAME.getValue())) {
            itemWriters.add(new UrlResultsFileWriter(outputPath, PatternEnum.NAME, new UrlResultsNamesAggregator(), messagingTemplate));
        }

        compWriter.setDelegates(itemWriters);

        return compWriter;
    }


}
