package com.crodriguez.test.javasmetest.batch.aggregator;

import com.crodriguez.test.javasmetest.dto.batch.UrlResultsDTO;
import org.springframework.batch.item.file.transform.LineAggregator;

/**
 * Line aggregator used to define how should be read the hash tags found in the processing step.
 */
public class UrlResultsHashAggregator implements LineAggregator<UrlResultsDTO> {

    @Override
    public String aggregate(UrlResultsDTO item) {
        return String.join("\n", item.getHashtags());
    }
}
