package com.crodriguez.test.javasmetest.batch.writer;

import com.crodriguez.test.javasmetest.constants.URLs;
import com.crodriguez.test.javasmetest.dto.batch.UrlResultsDTO;
import com.crodriguez.test.javasmetest.dto.notification.NotificationDTO;
import com.crodriguez.test.javasmetest.enumeration.PatternEnum;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.core.io.FileSystemResource;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * File writer used to generate the different output files for the processing.job. This writer generates one file by url
 * processed according to the pattern specified as argument in the instantiation.
 */
public class UrlResultsFileWriter implements ItemStream, ItemWriter<UrlResultsDTO> {

    private Map<String, FlatFileItemWriter<UrlResultsDTO>> writers = new HashMap<>();
    private ExecutionContext executionContext;
    private String outputPath;
    private PatternEnum pattern;
    private LineAggregator<UrlResultsDTO> lineAggregator;
    private SimpMessagingTemplate messagingTemplate;

    public UrlResultsFileWriter(String outputPath, PatternEnum pattern, LineAggregator<UrlResultsDTO> lineAggregator, SimpMessagingTemplate messagingTemplate) {
        this.outputPath = outputPath;
        this.pattern = pattern;
        this.lineAggregator = lineAggregator;
        this.messagingTemplate = messagingTemplate;
    }

    @Override
    public void write(List<? extends UrlResultsDTO> items) throws Exception {
        for (UrlResultsDTO item : items) {
            Path fileName = Paths.get(outputPath, item.getUrlClean(), pattern.getValue().toLowerCase() + ".txt");
            FlatFileItemWriter<UrlResultsDTO> ffiw = getFlatFileItemWriter(item, fileName);
            ffiw.write(Arrays.asList(item));
            NotificationDTO fileProcessed = new NotificationDTO(fileName.toString());
            messagingTemplate.convertAndSend(URLs.NOTIFICATIONS_URL, fileProcessed);
        }
    }

    private FlatFileItemWriter<UrlResultsDTO> getFlatFileItemWriter(UrlResultsDTO item, Path fileName) {
        FlatFileItemWriter<UrlResultsDTO> writer = new FlatFileItemWriter<>();
        writer.setLineAggregator(lineAggregator);
        writer.setResource(new FileSystemResource(fileName.toString()));
        writer.open(executionContext);
        writers.put(outputPath, writer);
        return writer;
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
        this.executionContext = executionContext;
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {
    }

    @Override
    public void close() throws ItemStreamException {
        for (FlatFileItemWriter f : writers.values()) {
            f.close();
        }
    }
}
