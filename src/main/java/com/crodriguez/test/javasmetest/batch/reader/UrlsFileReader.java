package com.crodriguez.test.javasmetest.batch.reader;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.PassThroughLineMapper;
import org.springframework.core.io.FileSystemResource;

import java.nio.file.Paths;

/**
 * File reader used to read the lines of the input file one by one.
 */
public class UrlsFileReader extends FlatFileItemReader<String> {

    public UrlsFileReader(String filePath) {
        super.setResource(new FileSystemResource(Paths.get(filePath).toString()));
        super.setLineMapper(new PassThroughLineMapper());
    }
}
