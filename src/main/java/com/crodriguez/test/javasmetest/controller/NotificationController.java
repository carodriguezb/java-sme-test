package com.crodriguez.test.javasmetest.controller;

import com.crodriguez.test.javasmetest.dto.notification.NotificationDTO;
import com.crodriguez.test.javasmetest.constants.URLs;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller(value = "/notification")
public class NotificationController {

    @MessageMapping(URLs.WEB_SOCKET_ENDPOINT_URL)
    @SendTo(URLs.NOTIFICATIONS_URL)
    public NotificationDTO sendFileProcessedNotification(NotificationDTO fileProcessed) {
        return fileProcessed;
    }
}
