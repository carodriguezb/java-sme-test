package com.crodriguez.test.javasmetest.controller;

import com.crodriguez.test.javasmetest.constants.URLs;
import com.crodriguez.test.javasmetest.dto.form.FileUploadFormDTO;
import com.crodriguez.test.javasmetest.enumeration.PatternEnum;
import com.crodriguez.test.javasmetest.service.ProcessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;
import java.nio.file.Paths;

@Controller
public class FileController {

    private Logger logger = LoggerFactory.getLogger(FileController.class);

    private final ProcessService processService;

    @Autowired
    public FileController(ProcessService processService) {
        this.processService = processService;
    }

    @GetMapping(value = "/file")
    public String filePage(FileUploadFormDTO fileUploadFormDTO, BindingResult bindingResult, Model model) {
        model.addAttribute("patterns", PatternEnum.values());
        return URLs.FILE_TEMPLATE;
    }

    @PostMapping(value = "/file")
    public String uploadAndProcessFile(@Valid @ModelAttribute FileUploadFormDTO fileUploadformDTO,
                                       BindingResult bindingResult,
                                       Model model, RedirectAttributes redirectAttributes) {

        logger.debug("Received file upload request");

        if (bindingResult.hasErrors()) {
            model.addAttribute("patterns", PatternEnum.values());
            model.addAttribute("fileUploadMessageError", "File " + fileUploadformDTO.getFile().getOriginalFilename() + " couldn't be uploaded.");
            return URLs.FILE_TEMPLATE;
        } else {
            if (processService.saveAndProcessFile(fileUploadformDTO)) {
                redirectAttributes.addFlashAttribute("fileUploadMessageSuccess", "File " + fileUploadformDTO.getFile().getOriginalFilename() + " successfully uploaded. Process will start shortly.");
            } else {
                redirectAttributes.addFlashAttribute("fileUploadMessageError", "File " + fileUploadformDTO.getFile().getOriginalFilename() + " couldn't be uploaded.");
            }
        }

        return "redirect:" + URLs.FILE_TEMPLATE;
    }

    @ResponseBody
    @GetMapping(value = "/files/download/{file_name}")
    public FileSystemResource downloadFile(@PathVariable("file_name") String fileName) {
        String filePath = Paths.get(fileName.replaceAll("_", "/")).toString();
        return new FileSystemResource(filePath);
    }
}
