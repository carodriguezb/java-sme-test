package com.crodriguez.test.javasmetest.controller;

import com.crodriguez.test.javasmetest.constants.URLs;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping("/")
    public String indexPage() {
        return "redirect:" + URLs.FILE_TEMPLATE;
    }
}
