package com.crodriguez.test.javasmetest.enumeration;

/**
 * Contains the types of patterns available for processing.
 */
public enum PatternEnum {

    ACCOUNT("ACCOUNT", "twitter accounts", false),
    HASH("HASH", "hash tags", false),
    NAME("NAME", "proper names", true);

    private String value;
    private String label;
    private boolean disabled;

    PatternEnum(String value, String label, boolean disabled) {
        this.value = value;
        this.label = label;
        this.disabled = disabled;
    }

    public String getValue() {
        return value;
    }

    public String getLabel() {
        return label;
    }

    public boolean isDisabled() {
        return disabled;
    }
}
