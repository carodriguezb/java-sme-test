package com.crodriguez.test.javasmetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSmeTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSmeTestApplication.class, args);
	}
}
