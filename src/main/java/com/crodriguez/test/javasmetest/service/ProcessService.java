package com.crodriguez.test.javasmetest.service;

import com.crodriguez.test.javasmetest.batch.ProcessUrlsFileBatch;
import com.crodriguez.test.javasmetest.dto.form.FileUploadFormDTO;
import com.crodriguez.test.javasmetest.enumeration.PatternEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Handles all the operations related to the file processing.
 */
@Service
public class ProcessService {

    private final StorageService storageService;
    private final BatchJobsRunner batchJobsRunner;


    @Autowired
    public ProcessService(StorageService storageService, BatchJobsRunner batchJobsRunner) {
        this.storageService = storageService;
        this.batchJobsRunner = batchJobsRunner;
    }

    /**
     * Calls the persist file logic ({@link StorageService#saveFile(MultipartFile)}) and starts the job for
     * processing the file ({@link ProcessUrlsFileBatch}).
     *
     * @param fileUploadFormDTO {@link FileUploadFormDTO} with the file content.
     * @return true if the file was saved correctly, false otherwise.
     */
    public boolean saveAndProcessFile(FileUploadFormDTO fileUploadFormDTO) {
        String generatedFilePath = storageService.saveFile(fileUploadFormDTO.getFile());

        if (generatedFilePath != null) {

            String patterns = Arrays.stream(
                    fileUploadFormDTO.getPatternsToProcess())
                    .map(PatternEnum::getValue)
                    .collect(Collectors.joining("|"));

            batchJobsRunner.startProcessUrlsJob(generatedFilePath, patterns);

            return true;
        }

        return false;
    }

}
