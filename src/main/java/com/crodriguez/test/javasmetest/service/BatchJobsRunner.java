package com.crodriguez.test.javasmetest.service;

import com.crodriguez.test.javasmetest.batch.ProcessUrlsFileBatch;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class BatchJobsRunner {

    private final JobLauncher jobLauncher;
    private final Job job;

    @Autowired
    public BatchJobsRunner(JobLauncher jobLauncher, @Qualifier("ProcessUrlsFileJob") Job job) {
        this.jobLauncher = jobLauncher;
        this.job = job;
    }

    /**
     * Async method called to start the processing job ({@link ProcessUrlsFileBatch}).
     *
     * @param inputFilePath String with the path to the input file to process.
     * @param patterns      String with the patterns to process concatenated.
     */
    @Async
    public void startProcessUrlsJob(String inputFilePath, String patterns) {
        try {
            String filePath = inputFilePath.substring(0, inputFilePath.lastIndexOf(File.separator));
            JobParameters jobParameters = new JobParametersBuilder()
                    .addString(ProcessUrlsFileBatch.PARAM_INPUT_FILE_PATH, inputFilePath)
                    .addString(ProcessUrlsFileBatch.PARAM_OUTPUT_PATH, filePath)
                    .addString(ProcessUrlsFileBatch.PARAM_PATTERNS, patterns)
                    .toJobParameters();
            jobLauncher.run(job, jobParameters);
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
            e.printStackTrace();
        }
    }
}
