package com.crodriguez.test.javasmetest.service;

import com.crodriguez.test.javasmetest.dto.form.FileUploadFormDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Handles all the operation related to persistence and load of files.
 */
@Service
public class StorageService {

    private Logger logger = LoggerFactory.getLogger(StorageService.class);

    @Value("${app.upload-folder}")
    private String UPLOADED_FOLDER;

    /**
     * Saves a {@link MultipartFile} in a folder defined by the application property {@link #UPLOADED_FOLDER}
     * and the current date. Check {@link #createUploadFolder()}.
     *
     * @param file {@link FileUploadFormDTO} with the file data.
     * @return true if the file was saved, false otherwise.
     */
    public String saveFile(MultipartFile file) {

        Path filePath;
        try {
            String uploadFolder = createUploadFolder();
            logger.debug("Saving file in folder {}", uploadFolder);
            byte[] bytes = file.getBytes();
            filePath = Paths.get(uploadFolder, file.getOriginalFilename());
            Files.write(filePath, bytes);
            logger.debug("File saved in path {}", filePath.toString());
        } catch (IOException e) {
            logger.error("There was an error saving the file " + e.getMessage());
            e.printStackTrace();
            return null;
        }

        return filePath.toString();
    }

    /**
     * Creates a folder in the path specified by the application property {@link #UPLOADED_FOLDER} and the current
     * date using the format yyyyMMddHHmmss.
     *
     * @return the path of the folder created.
     * @throws IOException in case something goes wrong in the folders creation.
     */
    private String createUploadFolder() throws IOException {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String dateString = dateFormat.format(new Date());
        Path uploadFolderPath = Paths.get(UPLOADED_FOLDER, dateString);
        Files.createDirectories(uploadFolderPath);
        return uploadFolderPath.toString();
    }

    public void downloadFile() {
    }

}
