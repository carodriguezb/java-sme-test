package com.crodriguez.test.javasmetest.dto.batch;

import com.crodriguez.test.javasmetest.batch.ProcessUrlsFileBatch;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;

/**
 * Used to store the results of the process done by the job {@link ProcessUrlsFileBatch}
 */
public class UrlResultsDTO {

    private String url;
    private LinkedList<String> hashtags = new LinkedList<>();
    private LinkedList<String> accounts = new LinkedList<>();
    private LinkedList<String> names = new LinkedList<>();

    public String getUrl() {
        return url;
    }

    public String getUrlClean() throws URISyntaxException {
        return (new URI(url)).getHost();
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public LinkedList<String> getHashtags() {
        return hashtags;
    }

    public void setHashtags(LinkedList<String> hashtags) {
        this.hashtags = hashtags;
    }

    public LinkedList<String> getAccounts() {
        return accounts;
    }

    public void setAccounts(LinkedList<String> accounts) {
        this.accounts = accounts;
    }

    public LinkedList<String> getNames() {
        return names;
    }

    public void setNames(LinkedList<String> names) {
        this.names = names;
    }
}
