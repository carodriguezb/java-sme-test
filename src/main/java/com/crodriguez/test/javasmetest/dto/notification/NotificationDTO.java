package com.crodriguez.test.javasmetest.dto.notification;

/**
 * Contains the text send as notification to the UI websocket.
 */
public class NotificationDTO {

    private String message;

    public NotificationDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
