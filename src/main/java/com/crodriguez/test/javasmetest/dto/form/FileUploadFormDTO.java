package com.crodriguez.test.javasmetest.dto.form;

import com.crodriguez.test.javasmetest.enumeration.PatternEnum;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Definition of the model used in the view where users upload the file to process.
 */
public class FileUploadFormDTO {

    @NotEmpty(message = "Must select at least one option")
    private PatternEnum[] patternsToProcess;

    @NotNull
    private MultipartFile file;

    public PatternEnum[] getPatternsToProcess() {
        return patternsToProcess;
    }

    public void setPatternsToProcess(PatternEnum[] patternsToProcess) {
        this.patternsToProcess = patternsToProcess;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
